<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>ARCHIVOS HIDROITUANGO</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/swiper.min.css">
  <link rel="stylesheet" href="../css/main.6.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <?php
    //Find last file time
    $files = scandir ("../subestacion/");
    $firstFile = $directory . $files[2];

    $files_descarga = scandir ("../descarga/");
    $firstFile_descarga = $directory . $files_descarga[2];

    $files_embalse = scandir ("../embalse/");
    $firstFile_embalse = $directory . $files_embalse[2];
  ?>

  <header>
    <h1><a href="/">Archivo histórico de imágenes de Hidroituango</a></h1>
  </header>
  <div id="main">

    <div id="player">
      <?php
        // import AWS sdk
        require '../aws/aws-autoloader.php';

        // Initialize S3
        $s3 = new Aws\S3\S3Client([
          'region'  => 'us-east-1',
          'version' => 'latest',
          'endpoint_discovery' => [
              'enabled' => true,
              'cache_limit' => 3000
          ],
          'credentials' => [
              'key'    => "AKIAVH27YZA7A4YUNDA7",
              'secret' => "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"
          ]
        ]);

        $tz = 'America/Bogota';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp

        $camara = $_GET["camara"];
        if( $camara == 'subestacion'){
          $camara = 'subestacion';
        }elseif ($camara == 'descarga') {
          $camara = 'descarga';
        }elseif ($camara == 'embalse') {
          $camara = 'embalse';
        }else{
          $camara = 'embalse';
        }

        function validateDate($date, $format = 'Y-m-d\TH:i')
        {
            $timestamp2 = strtotime($date);
            $newformat = date('Y-m-d\TH:i',$timestamp2);
            // echo "<h2>NEWFORMAT ".$newformat."</h2>";
            $d = DateTime::createFromFormat($format, $newformat);
            return $d && $d->format($format) == $newformat;
        }

        function longestCommonSubstring($words){
          $words = array_map('strtolower', array_map('trim', $words));
          $sort_by_strlen = create_function('$a, $b', 'if (strlen($a) == strlen($b)) { return strcmp($a, $b); } return (strlen($a) < strlen($b)) ? -1 : 1;');
          usort($words, $sort_by_strlen);
          // We have to assume that each string has something in common with the first
          // string (post sort), we just need to figure out what the longest common
          // string is. If any string DOES NOT have something in common with the first
          // string, return false.
          $longest_common_substring = array();
          $shortest_string = str_split(array_shift($words));

          while (sizeof($shortest_string)) {
              array_unshift($longest_common_substring, '');
              foreach ($shortest_string as $ci => $char) {
                  foreach ($words as $wi => $word) {
                      if (!strstr($word, $longest_common_substring[0] . $char)) {
                          // No match
                          break 2;
                      } // if
                  } // foreach
                  // we found the current char in each word, so add it to the first longest_common_substring element,
                  // then start checking again using the next char as well
                  $longest_common_substring[0].= $char;
              } // foreach
              // We've finished looping through the entire shortest_string.
              // Remove the first char and start all over. Do this until there are no more
              // chars to search on.
              array_shift($shortest_string);
          }
          // If we made it here then we've run through everything
          usort($longest_common_substring, $sort_by_strlen);
          return array_pop($longest_common_substring);
      }

      function customSearch($keyword, $arrayToSearch){
        foreach($arrayToSearch as $key => $arrayItem){
            if( stristr( $arrayItem, $keyword ) ){
                return $key;
            }
        }
      }

        $fechainicio = $_GET["fechainicio"];
        $fechafin = $_GET["fechafin"];
        // echo "<h2>".$camara." / de ".$fechainicio." a ".$fechafin."</h2>";
        if( validateDate( $fechainicio ) ){
          $timestamp3 = strtotime($fechainicio);
          $newformat2 = date('Y-m-d\TH:i',$timestamp3);
          $fechainicio = $newformat2;
        }else{
          $fechainicio = $dt->format('Y-m-d\TH:i');
        }

        if( validateDate( $fechafin ) ){
          $timestamp4 = strtotime($fechafin);
          $newformat3 = date('Y-m-d\TH:i',$timestamp4);
          $fechafin = $newformat3;
        }else{
          $fechafin = $dt->format('Y-m-d\TH:i');
        }
        // echo "<h2>".$camara." / de ".$fechainicio." a ".$fechafin."</h2>";

        if( !$_GET["fechainicio"] ){
          $newdt = $dt;
          $newdt = $newdt->modify('-3 Hours');
          $fechainicio = $newdt->format('Y-m-d\TH:i');
        }

        if( !$_GET["fechafin"] ){
          $dt = new DateTime("now", new DateTimeZone($tz));
          $fechafin = $dt->format('Y-m-d\TH:i');
        }
        if( strtotime($fechafin) < strtotime($fechainicio) ){
          $fechafin = $fechainicio;
        }

        $intervalos = array(
            "PT1M",
            "PT3M",
            "PT5M",
            "PT10M",
            "PT30M",
            "PT1H",
            "PT3H",
            "PT6H",
            "PT12H",
            "PT24H",
            "P3D",
            "P7D",
            "P15D",
            "P1M"
        );

        $intervalo = $_GET["intervalo"];
        if( is_string( $intervalo ) && in_array( $intervalo, $intervalos ) ){
          $intervalo = $_GET["intervalo"];
        }else{
          $intervalo = "PT1M";
        }

        $fpm = intval( $_GET["fpm"] );
        if( is_int($fpm) && $fpm > 0 && $fpm < 481 ){
          $fpm = intval( $_GET["fpm"] );
        }else{
          $fpm = 180;
        }


        $begin = new DateTime( $fechainicio );
        $begin = $begin->modify( '-1 minute' );
        $end = new DateTime( $fechafin );
        $end = $end->modify( '+1 minute' );
        $interval = new DateInterval( $intervalo );
        $daterange = new DatePeriod($begin, $interval ,$end);
        $fechas = Array();
        $fechas_disponibles = Array();
        $imagenes = Array();

        if( $daterange ){
          // echo "<h3>Fechas solicitadas: </h3><ul>";
          foreach($daterange as $date){
            $fechas[] = $date->format("Y/m/d/Ymd-Hi");
            // echo "<li>".$camara."/".$date->format("Y/m/d/Ymd-Hi") . "</li>";
          }
          // echo "</ul>";

          // Use the high-level iterators (returns ALL of your objects).
          try {
              $common_prefix = longestCommonSubstring( $fechas );
              // echo "<h1>Common prefix: ".$common_prefix."</h1>";
              $prefix = $camara."/".$common_prefix;
              // echo "<h1>prefix: ".$prefix."</h1>";
              $results = $s3->getPaginator('ListObjects', [
                  'Bucket' => 'hidroituango',
                  'Prefix' => $prefix
              ]);

              foreach ($results as $result) {
                  //echo "<hr>";
                  // print_r( $result );
                  foreach ($result['Contents'] as $object) {
                      //echo $object['Key'] . PHP_EOL;
                      $fechas_disponibles[] = $object['Key'];
                  }
              }
          } catch (S3Exception $e) {
              echo $e->getMessage() . PHP_EOL;
          }

          // echo "<h3>Fechas disponibles: </h3><ul>";
          // foreach($fechas_disponibles as $date){
            // echo "<li>".$date."</li>";
          // }
          // echo "</ul>";

          foreach($fechas as $searchword){
            // echo "<h2>MATCHES:</h2>";
            $imagenes[] = $fechas_disponibles[ customSearch($searchword, $fechas_disponibles) ];
            //echo $fechas_disponibles[ customSearch($searchword, $fechas_disponibles) ];
          }
        }
      ?>
      <!-- Slider main container -->
      <div class="swiper-container">
          <div class="play playing">Reproducir >></div>
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
              <!-- Slides -->
              <?php
                $count = 0;
                foreach ($imagenes as $img) {
                  if( $img != ''){
                    $count++;
                    echo '<div class="swiper-slide">';
                    echo "<img data-src='https://imagenes.hidroituango.live/".$img."' width='1920' height='1080' class='swiper-lazy'/><div class='swiper-lazy-preloader'></div></div>";
                  }
                }
              ?>
          </div>
          <div class="swiper-pagination swiper-pagination-white"></div>
          <div id="playpause"></div>
          <div class="swiper-button-prev swiper-button-white"></div>
          <div class="swiper-button-next swiper-button-white"></div>
      </div>
    </div>

    <div id="viewlapse">

      <h2>Ver intervalo de tiempo:</h2>
      <form method="GET">
        <label>Cámara:
          <select name="camara">
            <option <?php if( $camara == 'subestacion' ){ echo "selected"; } ?> value="subestacion">Subestacion</option>
            <option <?php if( $camara == 'descarga' ){ echo "selected"; } ?> value="descarga">Descarga</option>
            <option <?php if( $camara == 'embalse' ){ echo "selected"; } ?> value="embalse">Embalse</option>
          </select>
        </label>
        <label>Fecha y hora inicial:<br />
          <input type="datetime-local" name="fechainicio" step="1" min="2019-06-06T00:00" max="<?php echo $dt->format('Y-m-d\TH:i'); ?>" value="<?php echo $fechainicio ; ?>">
        </label>
        <label>Fecha y hora final:<br />
           <input type="datetime-local" name="fechafin" step="1" min="2019-06-06T00:00" max="<?php echo $dt->format('Y-m-d\TH:i'); ?>" value="<?php echo $fechafin; ?>">
        </label>
        <label>Intervalo:
          <select name="intervalo">
            <option value="PT1M" <?php if( $intervalo == "PT1M" ){ echo "selected"; } ?>>1 minuto</option>
            <option value="PT3M" <?php if( $intervalo == "PT3M" ){ echo "selected"; } ?>>3 minutos</option>
            <option value="PT5M" <?php if( $intervalo == "PT5M" ){ echo "selected"; } ?>>5 minutos</option>
            <option value="PT10M" <?php if( $intervalo == "PT10M" ){ echo "selected"; } ?>>10 minutos</option>
            <option value="PT30M" <?php if( $intervalo == "PT30M" ){ echo "selected"; } ?>>30 minutos</option>
            <option value="PT1H" <?php if( $intervalo == "PT1H" ){ echo "selected"; } ?>>1 hora</option>
            <option value="PT3H" <?php if( $intervalo == "PT3H" ){ echo "selected"; } ?>>3 horas</option>
            <option value="PT6H" <?php if( $intervalo == "PT6H" ){ echo "selected"; } ?>>6 horas</option>
            <option value="PT12H" <?php if( $intervalo == "PT12H" ){ echo "selected"; } ?>>12 horas</option>
            <option value="PT24H" <?php if( $intervalo == "PT24H" ){ echo "selected"; } ?>>1 día</option>
            <option value="P3D" <?php if( $intervalo == "P3D" ){ echo "selected"; } ?>>3 días</option>
            <option value="P7D" <?php if( $intervalo == "P7D" ){ echo "selected"; } ?>>7 días</option>
            <option value="P15D" <?php if( $intervalo == "P15D" ){ echo "selected"; } ?>>15 días</option>
            <option value="P1M" <?php if( $intervalo == "P1M" ){ echo "selected"; } ?>>1 mes</option>
          </select>
        </label>
        <label>Cuadros por minuto:
          <select name="fpm">
            <option <?php if( $fpm == 480 ){ echo "selected"; }?>>480</option>
            <option <?php if( $fpm == 360 ){ echo "selected"; }?>>360</option>
            <option <?php if( $fpm == 240 ){ echo "selected"; }?>>240</option>
            <option <?php if( $fpm == 180 ){ echo "selected"; }?>>180</option>
            <option <?php if( $fpm == 120 ){ echo "selected"; }?>>120</option>
            <option <?php if( $fpm == 90 ){ echo "selected"; }?>>90</option>
            <option <?php if( $fpm == 60 ){ echo "selected"; }?>>60</option>
            <option <?php if( $fpm == 30 ){ echo "selected"; }?>>30</option>
            <option <?php if( $fpm == 15 ){ echo "selected"; }?>>15</option>
            <option <?php if( $fpm == 10 ){ echo "selected"; }?>>10</option>
            <option <?php if( $fpm == 5 ){ echo "selected"; }?>>5</option>
            <option <?php if( $fpm == 1 ){ echo "selected"; }?>>1</option>
          <select>
        </label>
        <input type="submit" value="Ver intervalo" />
      </form>
    </div>
  </div>
  <script src="../js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="../js/plugins.6.js"></script>
  <script src="../js/swiper.min.js"></script>
  <script src="../js/main.6.js"></script>

  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-141669007-1', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
  <script>

  var playlink = $('.play')
  var images_loaded = 0;
  var playing = true;
  $(function () {
    var mySwiper = new Swiper ('.swiper-container', {
      preloadImages: false,
      direction: 'horizontal',
      loop: true,
      speed: 0,
      autoHeight: true,
      allowTouchMove: false,

      lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 30,
      },

      pagination: {
        el: '.swiper-pagination',
        type: 'fraction'
      },

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      keyboard: {
        enabled: true,
        onlyInViewport: false,
      },

      autoplay: {
        delay: <?php echo 60000/$fpm; ?>,
        disableOnInteraction: true
      },

      on: {
        slideChangeTransitionEnd: function(){
          //console.log("changed");
          updatePlaypause();
        }
      }
    });

    playlink.on('click', function(){
      var Swiper = document.querySelector('.swiper-container').swiper;
      if( $(this).hasClass('paused') ){
        Swiper.autoplay.start();
        updatePlaypause();
      }else{
        Swiper.autoplay.stop();
        updatePlaypause();
      }
    });

    function updatePlaypause(){
      var Swiper = document.querySelector('.swiper-container').swiper;
      if( Swiper.autoplay.running ){
        if( !playing ){ return; }
        playlink.text('Pausar ||').removeClass("paused").addClass("playing");
        playing = false;
      }else{
        if( playing ){ return; }
        playlink.text('Reproducir >>').removeClass("playing").addClass("paused");
        playing = true;
      }
    }
  });

  </script>
  <!--

    Si estás buscando los créditos los encuentras en: https://hidroituango.live/humans.txt

  -->
</body>

</html>
