<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>ARCHIVOS HIDROITUANGO</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.6.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body class="home">
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <?php
    //Find last file time
    $files = scandir ("subestacion/");
    $firstFile = $files[2];

    $files_descarga = scandir ("descarga/");
    $firstFile_descarga = $files_descarga[2];

    $files_embalse = scandir ("embalse/");
    $firstFile_embalse = $files_embalse[2];
  ?>

  <header>
    <h1>Archivo histórico de imágenes de Hidroituango</h1>
    <span id="updater">Actualizando en: <span id="counter">60</span></span>
  </header>
  <div id="main">
    <div id="camaras">
      <div id="camara_subestacion">
        <h2>Cámara Subestación</h2>
        <img src="subestacion/<?php echo $firstFile; ?>" />
        <ul>
          <h3><a href="https://hidroituango.live/archivo/subestacion/">Ver archivos de esta cámara</a></h3>
        </ul>
      </div>

      <div id="camara_descarga">
        <h2>Cámara Descarga</h2>
        <img src="descarga/<?php echo $firstFile_descarga; ?>" />
        <ul>
          <h3><a href="https://hidroituango.live/archivo/descarga/">Ver archivos de esta cámara</a></h3>
        </ul>
      </div>

      <div id="camara_embalse">
        <h2>Cámara Embalse</h2>
        <img src="embalse/<?php echo $firstFile_embalse; ?>" />
        <ul>
          <h3><a href="https://hidroituango.live/archivo/embalse/">Ver archivos de esta cámara</a></h3>
        </ul>
      </div>
    </div>

    <div id="viewlapse">
      <?php
        $tz = 'America/Bogota';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      ?>
      <h2>Ver intervalo de tiempo:</h2>
      <form action="/intervalo" method="GET">
        <label>Cámara:
          <select name="camara">
            <option value="subestacion" selected>Subestacion</option>
            <option value="descarga">Descarga</option>
            <option value="embalse">Embalse</option>
          </select>
        </label>
        <label>Fecha y hora inicial:<br />
          <?php
            $dt_ini = new DateTime( "now", new DateTimeZone($tz) );
            $dt_ini = $dt_ini->modify('-3 hours');
          ?>
          <input type="datetime-local" name="fechainicio" step="1" min="2019-06-06T00:00" max="<?php echo $dt->format('Y-m-d\TH:i'); ?>" value="<?php echo $dt_ini->format('Y-m-d\TH:i'); ?>">
        </label>
        <label>Fecha y hora final:<br />
           <input type="datetime-local" name="fechafin" step="1" min="2019-06-06T00:00" max="<?php echo $dt->format('Y-m-d\TH:i'); ?>" value="<?php echo $dt->format('Y-m-d\TH:i'); ?>">
        </label>
        <label>Intervalo:
          <select name="intervalo">
            <option value="PT1M" selected>1 minuto</option>
            <option value="PT3M">3 minutos</option>
            <option value="PT5M">5 minutos</option>
            <option value="PT10M">10 minutos</option>
            <option value="PT30M">30 minutos</option>
            <option value="PT1H">1 hora</option>
            <option value="PT3H">3 horas</option>
            <option value="PT6H">6 horas</option>
            <option value="PT12H">12 horas</option>
            <option value="PT24H">1 día</option>
            <option value="P3D">3 días</option>
            <option value="P7D">7 días</option>
            <option value="P15D">15 días</option>
            <option value="P1M">1 mes</option>
          </select>
        </label>
        <label>Cuadros por minuto:
          <select name="fpm">
            <option>480</option>
            <option>360</option>
            <option>240</option>
            <option selected>180</option>
            <option>120</option>
            <option>90</option>
            <option>60</option>
            <option>30</option>
            <option>15</option>
            <option>10</option>
            <option>5</option>
            <option>1</option>
          <select>
        </label>
        <input type="submit" value="Ver intervalo" />
      </form>
    </div>
  </div>
  <footer>
    <p></p>
  </footer>
  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="js/plugins.6.js"></script>
  <script src="js/main.6.js"></script>

  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-141669007-1', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
  <!--

    Si estás buscando los créditos los encuentras en: https://hidroituango.live/humans.txt



  <div style="font-size:16px;margin:0 auto;width:300px" class="blockchain-btn" data-address="1CRycCzZq4rbcJN7oNjeBzuRh7ZJKjb3YX" data-shared="false">
  <div class="blockchain stage-begin" style="text-align:center;">
      <img src="https://blockchain.info/Resources/buttons/donate_64.png"/>
      <p>Este es un proyecto desarrollado voluntariamente <br>si quieres apoyar puedes donar<br> a la siguiente dirección Bitcoin.</p>
      <img width="250" style="margin:15px" src="img/1CRycCzZq4rbcJN7oNjeBzuRh7ZJKjb3YX.png"/>
      <small style="color:#fff;font-weight:bold;">1CRycCzZq4rbcJN7oNjeBzuRh7ZJKjb3YX</small>
  </div>-->

</div>
</body>

</html>
